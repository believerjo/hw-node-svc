/* istanbul ignore file */

const request = require("supertest");
const app = require("../app");

describe("Test HelloWorld Integration", () => {
  test("GET api/hello-world", async () => {
    const res = await request(app).get('/int-test/api/hello-world');
    expect(res.statusCode).toBe(200);
    expect(res.body.message).toBe("HelloWorld");
  });
});
