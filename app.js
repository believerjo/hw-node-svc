const express = require('express');
const api = require('./routes');

const app = express();
app.disable("x-powered-by")

app.use(express.json());
app.use(`/${process.env.ENV}/api`, api);

// High level error handling
app.use((req, res, next) => {
  const error = new Error('Not found');
  error.statusCode = 404;
  next(error);
});
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  const name = err.name || 'Error';
  res
    .status(statusCode)
    .json({ name, message: err.message });
});

module.exports = app;
