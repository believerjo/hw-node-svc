/* istanbul ignore file */

const helloWorldService = require('./hello-world');

describe('Test HelloWorld Service', () => {
  test('return hello-world', () => {
    expect(helloWorldService()).toBeDefined();
    expect(helloWorldService().message).toBe('HelloWorld');
  });
});
