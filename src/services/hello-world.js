const helloWorldService = () => {
  return {
    message: "HelloWorld"
  };
}

module.exports = helloWorldService;
