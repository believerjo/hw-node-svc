const helloWorldService = require("../services/hello-world.js");

helloWorldController = (req, res) => {
  return res.json(helloWorldService());
}

module.exports = helloWorldController;
