/* istanbul ignore file */

const helloWorldController = require('./hello-world');

describe('Test HelloWorld Controller', () => {

  const mock = jest.fn();
  mock.mockReturnValue({"message":"HelloWorld"});

  const req = {};
  const res = {
    json: mock
  };

  test('Test HelloWorld Controller returns message', () => {
    expect(helloWorldController(req, res).message).toBe("HelloWorld");
  });
});
