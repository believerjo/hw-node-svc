const router = require("express").Router();
const helloWorldController = require("./src/controllers/hello-world");

router.use("/hello-world", helloWorldController);

module.exports = router;
