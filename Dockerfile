FROM node:19-alpine3.15

WORKDIR /app

COPY package.json ./

RUN yarn ci

COPY . .

CMD [ "node", "index.js" ]
