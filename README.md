# Helloworld Service

This repo demonstrates a pipeline to deploy a simple web-app that returns a payload `{"message":"HelloWorld"}` for `GET` `/api/hello-world` API. The app is deployed onto an EC2 machine(free tier), various environments can be reached by prefix the api path with environment name, eg:

 Preview(feature branch) - `preview/{git_branch_name}/api/hello-world`
 
 Integration - `int/api/hello-world`
 
 QA - `qa/api/hello-world`
 
 Prod - `prod/api/hello-world`

 **Pipelines**:

 - feature branch - https://gitlab.com/believerjo/hw-node-svc/-/pipelines/719009692

 - master branch - https://gitlab.com/believerjo/hw-node-svc/-/pipelines/719022157

 **Sonar Cloud** - https://sonarcloud.io/summary/overall?id=believerjo_hw-node-svc


### Stages and Jobs
Pipeline is comprised of the following stages:

- **Build** : Responsible for unit test, integration test, docker build etc

- **Compliance** : Ensures quality gate passes, no vulnerabilities exist in containers etc

- **Integration** : Deploys container to preview and integration environments

- **Integration Verification** : Runs smoke test on integration environment

- **QA** : Deploys container to qa environment

- **QA Verification** : Runs smoke test on qa environment

- **Prod** : Deploys container to prod environment

- **Prod Verification** : Runs smoke test on prod environment

    
**Build Stage**
> `build_and_unit_test`: Docker in docker job that units, builds the container and pushes the container into gitlab container registry. Unit test coverage are exported for sonarqube to analyse

> `integration_test`: Runs the integration test located under `tests` folder

> `node_audit`: Runs `yarn audit`

> `nodejs_scan_sast`: Runs SAST on source code and fails the job/pipeline if vulnerabilities exist

**Compliance**
> `container_scanning`: Scans container built from **Build** stage and exports the reports

> `container_security_check`: Check the report from **container_scanning** job and fails job/pipeline if vulnerabilities exist

> `sonar_cloud_check`: Invokes sonar analysis(Sonar Cloud), receives coverage data from `build_and_unit_test` job and validates if quality gate is met. Failure results in job/pipeline failure

**Integration|QA**
> `deploy_preview`: Only runs on feature branch (non master branch). Deploy container to EC2. Expose service on http:<INSTANCE_PUBLIC_DNS>/preview/<GIT_BRANCH_NAME_SLUG>/api/hello-world

> `deploy_integration|deploy_qa`: Only runs on main branch. Deploy container to EC2. Expose service on http:<INSTANCE_PUBLIC_DNS>/[int|qa]/<GIT_BRANCH_NAME_SLUG>/api/hello-world

> `destroy_preview` : This is an auto stop job for `deploy_preview` to delete feature branch deploys to save resources. Only applies to feature branches.

**Integration|QA Verification**
> `smoke_test_integration|smoke_test_qa`: Smoke test for integration|qa environment, runs postman collection with assertions to shake down the environment post deployment

**Prod**
> `deploy_prod`: This is the only **manual step** in the pipeline, only allowed users can trigger this. Only runs on main branch. Deploy container to EC2. Expose service on http:<INSTANCE_PUBLIC_DNS>/prod/<GIT_BRANCH_NAME_SLUG>/api/hello-world

**Prod Verification**
> `smoke_test_prod`: Smoke test for prod environment, runs postman collection with assertions to shake down the environment post deployment
